import React from 'react';
import s from './TodoList.module.scss';
import Todo from '../Todo';
import PropTypes from 'prop-types'

const TodoList = ({todos, clickTodo, checkTodo, removeTodo}) => {
    if (todos.length)
        return (
            <ol className={s.todoList}>
                {
                    todos.map(item => 
                        <Todo 
                            key={item.id} 
                            {...item}
                            clickTodo={() => clickTodo(item.id)}
                            checkTodo={() => checkTodo(item.id)}
                            removeTodo={() => removeTodo(item.id)}
                        />
                    )
                }
            </ol>
        )
    else
        return (
                    <h3>Список задач пока что пуст. Добавьте первую задачу.</h3>
        )
}

TodoList.propTypes = {
    todos: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        checked: PropTypes.bool.isRequired,
        text: PropTypes.string.isRequired
    }).isRequired).isRequired
}

export default TodoList;