import React from 'react'
import s from './AddTodo.module.scss'

const AddTodo = ({input, newTodo, addTodo}) => {
    return (
        <div>
            <form className={s.addTodoForm} onSubmit={e => {
                e.preventDefault();
                if (!input.value.trim()) {
                    return;
                }
                newTodo = input.value
                addTodo(newTodo);
                input.value = '';
            }}>
                <input className={s.addTodoInput} ref={node => input = node} />
                <button className={s.addTodoButton} type="submit">Добавить</button>
            </form>
        </div>
    )
}

export default AddTodo;