import React from 'react'
import s from './Menu.module.scss'
import { Link } from 'react-router-dom'

const menuItems = [
    {
        path: '/',
        name: 'Home'
    },
    {
        path: '/todos',
        name: 'Todos'
    }
]

const Menu = () => {
    return (
            <ul className={s.menu}>
                {
                    menuItems.map( (item, index) => (
                        <Link key={index} className={s.menuItem} to={item.path}>
                                {item.name}                            
                        </Link>
                    ))
                }
            </ul>
    )
}

export default Menu;