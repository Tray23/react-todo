import React from 'react';
import s from './TodoItem.module.scss'
import { Link } from 'react-router-dom';
import NotFound from '../../pages/NotFound'
import PropTypes from 'prop-types';

function TodoItem(props) {
    const todo = props.todoItem;
    if (todo) 
        return (        
            <div className={s.todo}>
                <p className={s.todoTitle}>Задача: <b>{todo.text}</b></p>
                <p className={s.todoChecked}>Статус: <b>{ todo.checked ? 'Выполнено' : 'Не выполнено' }</b></p>
                <Link className={s.linkStyle} to='/todos'>Вернуться к списку задач</Link>
            </div>
        );
    else
         return <NotFound />;
}

TodoItem.propTypes = {
    todoItem: PropTypes.object.isRequired
};

export default TodoItem;