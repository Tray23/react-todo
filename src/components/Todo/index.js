import React from 'react';
import PropTypes from 'prop-types';
import s from './Todo.module.scss';
import { Link } from 'react-router-dom'

const Todo = ({ id, checked, text, clickTodo, checkTodo, removeTodo}) => {
    let liClassName = s.todoItem
    let spanClassName = s.todoItemText + " " + (checked ? s.checked : '')
    let buttonClassName = s.todoItemButton
    return (
        <div>

            <li
                className={liClassName}
            >
                <Link to={`/todos/${id}`} className={spanClassName}
                onClick={clickTodo}>
                    {text}
                </Link>
                <button
                    className={buttonClassName}
                    onClick={checkTodo}
                >
                    { checked ? 'Снять метку' : 'Отметить  ' }
                </button>
                <button
                    className={buttonClassName}
                    onClick={removeTodo}
                >
                    Удалить
                </button>
            </li>
        </div>
    )
}

Todo.propTypes = {
    id: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    checked: PropTypes.bool.isRequired
}

export default Todo;