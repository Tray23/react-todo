import React from 'react'
import { Link } from 'react-router-dom'

const titleStyle = {
    textAlign: 'center'
}

const descriptionStyle = {
    color: 'gray'

}

const linkStyle = {
    textAlign: 'center',
    color: 'black'
}
const Home = () => {
    return (
        <div>
            <h1 style={titleStyle}>Добро пожаловать в React App</h1>
            <p style={descriptionStyle}>In nostrud veniam sint ut ipsum elit est id adipisicing officia. Eu quis eiusmod voluptate do ipsum incididunt do. 
                Adipisicing sit culpa sint elit aute nostrud esse quis laboris commodo fugiat esse sint.</p>
            <Link to='/todos' style={linkStyle}>Перейти к списку задач</Link>
        </div>
    );
}

export default Home;