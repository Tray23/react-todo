import React from 'react';
import AddTodo from '../containers/AddTodo';
import TodoList from '../containers/TodoList';
import TodoItem from '../containers/TodoItem';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';


function Todos() {
    return (
        <div>
            <Router>
                <Switch>
                    <Route exact path='/todos'>                                    
                        <AddTodo />
                        <TodoList />
                    </Route>
                    <Route path='/todos/:id'>
                        <TodoItem />
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default Todos;