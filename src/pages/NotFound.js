import React from 'react'
import { Link, useLocation } from 'react-router-dom'

const NotFound = () => {
    let location = useLocation();
    return (
        <div>
            <h1>Страница "{location.pathname}" не существует</h1>
            <Link to='/todos'>Вернуться к списку задач</Link>
        </div>
    );
}

export default NotFound;