import React from 'react';
import './App.css';
import Home from './pages/Home';
import Todos from './pages/Todos';
import NotFound from './pages/NotFound'
import Menu from './components/Menu';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App({store}) {
  return (
    <div className="App">      
      <Router>
        <Menu />
        <Switch>
          <Route path='/todos'>
            <Todos store={store}/>
          </Route>
          <Route exact path='/'>
            <Home />
          </Route> 
          <Route path='*'>
            <NotFound />
          </Route>
        </Switch>
      </Router>
        
    </div>
  );
}

export default App;
