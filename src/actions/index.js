let currentId = 1;

export const addTodo = text => ({
    type: 'ADD_TODO',
    id: currentId++,
    text
})

export const clickTodo = id => ({
    type: 'CLICK_TODO',
    id
})

export const checkTodo = id => ({
    type: 'CHECK_TODO',
    id
})

export const removeTodo = id => ({
    type: 'REMOVE_TODO',
    id
})