import { combineReducers } from 'redux'
import todos from './todos'
import todoItem from './common'

export default combineReducers({
    todos,
    todoItem
})