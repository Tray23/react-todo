const todoReducer = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return {
                        id: action.id,
                        text: action.text,
                        checked: false
                    };

        case 'CHECK_TODO':            
            if (state.id !== action.id) {
                return state;
            }
            return Object.assign({}, state, {
                checked: !state.checked
            });

        default:
            return state;
    }
}

const todos = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return [...state, todoReducer(undefined, action)];

        case 'CHECK_TODO':
            return state.map(todo => todoReducer(todo, action));

        case 'REMOVE_TODO':
            return state.filter(item => item.id !== action.id);

        default:
            return state;
    }
}

export default todos;