const todoItem = (state = -1, action) => {
    switch (action.type) {
        case 'CLICK_TODO':
            const activeTodoId = action.id;
            return {
                activeTodoId
            };

        default:
            return state;
        }
    }
            
export default todoItem;