import  { connect } from 'react-redux';
import TodoList from '../components/TodoList';
import { clickTodo, checkTodo, removeTodo } from '../actions';

const getVisibleTodos = (todos) => {
    return todos;
}

function mapStateToProps(state) {
    return {
        todos: getVisibleTodos(state.todos)
    };
}

function mapDispatchToProps(dispatch) {
    return {
        clickTodo: id => dispatch(clickTodo(id)),
        checkTodo: id => dispatch(checkTodo(id)),
        removeTodo: id => dispatch(removeTodo(id))
    };
}

export default connect (
    mapStateToProps,
    mapDispatchToProps
)(TodoList);