import { connect } from 'react-redux';
import TodoItem from '../components/TodoItem';

function getTodo(state) {
    return state.todos.find(item => item.id === state.todoItem.activeTodoId)
}

function mapStateToProps(state) {
    return {
        todoItem: getTodo(state)
    };
}

export default connect(
    mapStateToProps
)(TodoItem)